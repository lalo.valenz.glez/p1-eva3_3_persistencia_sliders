//
//  ViewController.swift
//  Eva3_3_persistencia_sliders
//
//  Created by TEMPORAL2 on 25/11/16.
//  Copyright © 2016 TEMPORAL2. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var sliders: [UISlider]!
    @IBOutlet var labels: [UILabel]!
    
    @IBAction func changeValue(sender: AnyObject) {
        for (var i = 0; i < sliders.count; i++){
            var slide: UISlider = sender as! UISlider
            switch slide {
            case sliders[0]:
                labels[0].text = "Slide 1: \(slide.value)"
                sliders[0].value = slide.value
                break
                
            case sliders[1]:
                labels[1].text = "Slide 2: \(slide.value)"
                sliders[1].value = slide.value
                break
                
            case sliders[2]:
                sliders[2].value = slide.value
                labels[2].text = "Slide 3: \(slide.value)"
                break
            case sliders[3]:
                labels[3].text = "Slide 4: \(slide.value)"
                sliders[3].value = slide.value
                break
            default:
                break
            }
        }

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let urlFile = leerUbicacion()
        if NSFileManager.defaultManager().fileExistsAtPath(urlFile.path!) {
            if let arr = NSArray(contentsOfURL: urlFile) as? [String] {
                for var i = 0; i < arr.count; i++ {
                    sliders[i].value = Float(arr[i])!
                    labels[i].text = arr[i]
                }
            }
        }
        
        let app = UIApplication.sharedApplication()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "applicationWillResignActive:", name: UIApplicationWillResignActiveNotification, object: app)
    }
    func applicationWillResignActive(notification: NSNotification) {
        let urlFile = leerUbicacion()
        let arreglo = (sliders as NSArray).valueForKey("value") as! NSArray
        arreglo.writeToURL(urlFile, atomically: true)
    }
    
    func leerUbicacion() -> NSURL {
        //recupera la ruta de los documentos dentro del dominio como app del usuario y a esa ruta se le pega el nombre del archivo
        let urls = NSFileManager.defaultManager().URLsForDirectory(.DocumentDirectory, inDomains: .UserDomainMask)
        return urls.first!.URLByAppendingPathComponent("data.plist")
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

